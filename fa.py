import numpy as np
import pandas as pd
from numpy import inf
import math
from itertools import groupby

def CalculateTime(jarak, kecepatan):
    return 0 if kecepatan == 0 else jarak/kecepatan

def GetDistance(data, row, col):
    return data.iloc[row-1][col-1]

def CalculateDistance(data, rute):
    totalDistance = 0

    if (set(rute_utama) <= set(rute)):
        return inf

    for i in range(len(rute)-1):
        distance = GetDistance(data, rute[i], rute[i+1])
        if (distance == 0):
            return inf #No edge found
        totalDistance += distance
        if (rute[i+1] == 9):
            return totalDistance #End Route
    return totalDistance

def PrintStatus(statusRoute, rute, time):
    print(statusRoute, rute, '\nTime: ', time, '\n\n')

def CalculateLightIntensity(data, kecepatan, rute):
    jarak = CalculateDistance(data, rute)
    if (jarak == inf):
        return 0
    time = CalculateTime(jarak, kecepatan)
    return 0 if time == 0 else 1/time

def GetLightIntensityFromAllFireflies(populasi, data, kecepatan):
    lights = []
    for rute in populasi:
        lights.append(CalculateLightIntensity(data, kecepatan, rute))
    return lights

def InitialPopulation(nPopulasi, nGen):
    populasi = []
    #populasi.append([i for i in range(1, nGen+1)]) # [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
    while (len(populasi) != nPopulasi):
        firefly=[1, 2, 3] # Probabilities route selalu 1,2,3 di awal node
        firefly.extend(np.random.choice(range(4,nGen+1), nGen-3, replace=False)) #Random permutasi, without replacement
        populasi.append(firefly)
        EvaluatePopulasi(populasi) # Remove duplicate firefly
    
    return populasi

def OrderPopulation(light, populasi):
    indeks = np.argsort(light, axis=0) #sorting ascending
    light_ = [light[idx] for idx in indeks]
    populasi_ = [populasi[idx] for idx in indeks]
    return light_, populasi_

def EvaluatePopulasi(populasi):
    return [k for k,v in groupby(sorted(populasi))]

### Main Program
np.seterr(divide='ignore', invalid='ignore')

# Init
data = pd.read_csv("datajarak.csv")
rute_utama = []
rute_utama.extend(range(1,10))
jarak_rute_utama = CalculateDistance(data, rute_utama)
kecepatan_rute_utama = 83.3333 # m/menit = 5km/jam
PrintStatus('Main Route: ', rute_utama, CalculateTime(jarak_rute_utama, kecepatan_rute_utama))

kecepatan_rute_alt = 333.333  # m/menit = 20km/jam
gamma = 0.01; # Absorption coefficient
beta0 = 1 # Attractiveness constant
alpha=1.0; # Randomness strength 0--1 (highly random)
theta=0.97; # Randomness reduction factor theta=10^(-5/tMax)
nPopulasi = 15 # Time Complexity = O(n!) = O(7!) = 5040
nGen = 18 # Dimension

lb=4
ub=18
scale = ub - lb

bestFirefly = []
solutionFound = False
populasi = InitialPopulation(nPopulasi, nGen)
print("Populasi init: ")
print(*populasi, sep = "\n")
tempPopulasi = []

light = GetLightIntensityFromAllFireflies(populasi, data, kecepatan_rute_alt)

light_, populasi_ = OrderPopulation(light, populasi)

loopingCount = 0

print("\nPopulasi ", loopingCount)
print(*populasi_, sep = "\n")

# best firefly check
if (light_[len(light_)-1] > 0):
    solutionFound = True
    bestFirefly = populasi_[len(populasi_)-1]
    
while (solutionFound == False):
    loopingCount = loopingCount + 1
    
    alpha *= theta
    populasi.extend(tempPopulasi)
    populasi = EvaluatePopulasi(populasi) # Remove duplicate firefly
    light = GetLightIntensityFromAllFireflies(populasi, data, kecepatan_rute_alt)
    light_, populasi_ = OrderPopulation(light, populasi)
    
    print("\nPopulasi loop", loopingCount)
    print(*populasi_, sep = "\n")

    # best firefly check
    if (light_[len(light_)-1] > 0):
        solutionFound = True
        bestFirefly = populasi_[len(populasi_)-1]
        break
    
    tempPopulasi = []
    
    for i in range(len(populasi_)):
        for j in range(i):
            if light_[j] >= light_[i]:
                selectedFirefly = populasi_[i]
                
                hasRoute = False
                hasRouteNext = False
                indexNoRoute = 0
                totalDistance = 0
                for k in range(len(selectedFirefly)-1):
                    distance = GetDistance(data, selectedFirefly[k], selectedFirefly[k+1])
                    if (distance == 0):
                        indexNoRoute = k+1
                        distance = GetDistance(data, selectedFirefly[indexNoRoute], selectedFirefly[indexNoRoute+1])
                        # if (distance == 0) :
                            #
                        # Swap
                    else:
                        hasRoute = True
                        # Swap
                
                r=np.sqrt(np.sum([k**2 for k in ([a-b for a,b in zip(populasi_[i], populasi_[j])])])) #Euclidean distance
                beta = beta0*math.exp(-gamma*(r**2))
                indexForSwap = int(scale * np.random.uniform(0,1,1) + lb)
                if alpha*np.random.uniform(0,1,1) > 0.5:
                    index2ForSwap = indexForSwap + int(beta) + lb
                else:
                    index2ForSwap = indexForSwap - int(beta) - lb
                if (index2ForSwap > ub):
                    selectedFirefly[indexForSwap-1], selectedFirefly[ub-1] =  selectedFirefly[ub-1], selectedFirefly[indexForSwap-1]
                elif (index2ForSwap < lb):
                    selectedFirefly[indexForSwap-1], selectedFirefly[lb-1] =  selectedFirefly[lb-1], selectedFirefly[indexForSwap-1]
                else:
                    selectedFirefly[indexForSwap-1], selectedFirefly[index2ForSwap-1] =  selectedFirefly[index2ForSwap-1], selectedFirefly[indexForSwap-1]
        
                tempPopulasi.append(selectedFirefly)

print('\nSummary Result')
print("Populasi init:")
print(*populasi, sep = "\n")
print("\nPopulasi akhir", loopingCount)
print(*populasi_, sep = "\n")
PrintStatus('\nMain Route: ', rute_utama, CalculateTime(jarak_rute_utama, kecepatan_rute_utama))
print('Best Firefly: ', bestFirefly)
alternate_route = bestFirefly[0:bestFirefly.index(9)+1]
PrintStatus('Altenative Route: ', alternate_route, CalculateTime(CalculateDistance(data, alternate_route), kecepatan_rute_alt))
